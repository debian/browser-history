BROWSER HISTORY
===============

See full docs in the HTML page browser-history.html or at the URL:

    http://www.inria.fr/koala/colas/browser-history

WARNING: browser-history works only on X, not on Windows or Macintosh.

Introduction

Browser-history came from the will to overcome a Netscape bug: there is no
global history, and if you close a window, its whole history is lost. For
people browsing lots of sites, having a possibility to track back where one
has been before means that you dont have to put everything in your bookmarks
file. If you are not sure if a site may be worth remebering, don't add it in
your bookmarks. If you need it later, just browse you history files.

Later, it came to our minds that this also could be a valuable add-on to
people writing experimental browsers, so they dont have to add this
functionality to their browser itself.

Browser-history is a small and efficient daemon. Real user services could be
built on top of the log files it maintains for more possibilities (graphical
representation, advanced search options, collective histories). It can be seen
as a quick-and-dirty hack wrt to the general solution of using a personal
proxy to provide this history and housekeeping facilities. But in the
meantime, it is easy to use and it works.

Author

Colas Nahaboo, Koala project. 
http://www.inria.fr/koala/colas

License

The X distribution license: you can do everything with this code (selling it,
modifying it), except suing me or using my name in your advertisements, or
expecting any kind of support or guarantee. 


Quick usage guide

Just run the program each time you launch your X server. 
Add the following line into your .xinitrc or equivalent startup file: 

     browser-history &
