# simplistic makefile
# possible flags: -DNO_STRSTR if error with undefined function strstr
# targets:
#linux: 
#sunsolaris: solaris
#sunos: bsd
#decalpha:
#SGI: cc


CC := gcc
CFLAGS := -O2 -g -Wall
LDFLAGS :=

browser-history: browser-history.c
	$(CC) -ansi $(CFLAGS) -I/usr/X11R6/include browser-history.c \
             -o browser-history -L/usr/X11R6/lib -lXmu -lX11 $(LDFLAGS)
all:
	$(CC)  -ansi $(CFLAGS) -I/usr/X11R6/include browser-history.c \
             -o browser-history -L/usr/X11R6/lib -lXmu -lX11 $(LDFLAGS)

cc:
	cc  -I/usr/X11R6/include -ansi -O -o browser-history browser-history.c \
	     -L/usr/X11R6/lib -lXmu -lX11

bsd:
	$(CC)  -I/usr/X11R6/include -ansi -O -Wall -DBSD browser-history.c -o browser-history \
	     -L/usr/X11R6/lib -lXmu -lX11

solaris:
	$(CC)  -I/usr/X11R6/include -ansi -O -Wall browser-history.c -o browser-history \
	     -R/usr/X11R6/lib \
	     -L/usr/X11R6/lib -lXmu -lX11 -ldl -lsocket -lnsl 


debug :
	$(CC) -I/usr/X11R6/include -DDEBUG -ansi -g -Wall browser-history.c -o browser-history \
	     -L/usr/X11R6/lib -lXmu -lX11 $(LDFLAGS)

tar:
	cd ..;tar cfvz browser-history/browser-history.tgz `cat browser-history/FILES_DIST`
shar:
	fshar < FILES | gzip > browser-history.shar.gz

clean:
	rm -f browser-history
